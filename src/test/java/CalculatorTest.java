import org.junit.jupiter.api.*;

public class CalculatorTest {

    private Calculator calculator = new Calculator();

    @Test
    public void testAdd() {
        Assertions.assertEquals(4, calculator.add(2,2), "2 and 2 did in fact become 4!");
    }
}
